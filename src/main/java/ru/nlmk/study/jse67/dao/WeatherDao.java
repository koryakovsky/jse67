package ru.nlmk.study.jse67.dao;

import org.springframework.stereotype.Repository;
import ru.nlmk.study.jse67.model.Weather;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class WeatherDao {

    @PersistenceContext
    private EntityManager em;

    public void save(Weather weather) {
        em.merge(weather);
    }
}
