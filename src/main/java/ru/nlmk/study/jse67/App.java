package ru.nlmk.study.jse67;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.nlmk.study.jse67.service.WeatherProcessingService;
import ru.nlmk.study.jse67.сonfig.AppConfig;

public class App {

    public static void main(String[] args) throws Exception {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(AppConfig.class);

        WeatherProcessingService processingService = ctx.getBean(WeatherProcessingService.class);

        while (true) {
            processingService.process();
            Thread.sleep(60 * 1000);
        }
    }
}
