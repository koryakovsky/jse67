package ru.nlmk.study.jse67.сonfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

@Import({PersistenceConfig.class, RabbitMQConfig.class})
@Configuration
@ComponentScan({"ru.nlmk.study.jse67.service"})
public class AppConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
