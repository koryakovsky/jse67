package ru.nlmk.study.jse67.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class CityWeatherRs {

    private Long id;

    @JsonProperty("weather_state_name")
    private String weatherStateName;

    @JsonProperty("wind_direction")
    private String windDirection;

    @JsonProperty("applicable_date")
    private Date date;

    @JsonProperty("min_temp")
    private Double minTemp;

    @JsonProperty("max_temp")
    private Double maxTemp;

    @JsonProperty("the_temp")
    private Double actualTemp;

    @JsonProperty("wind_speed")
    private Double windSpeed;

    @JsonProperty("air_pressure")
    private Double airPressure;

    private Double humidity;

    private Double visibility;

}
