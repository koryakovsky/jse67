package ru.nlmk.study.jse67.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CityWeatherByHoursRs {

    @JsonProperty("consolidated_weather")
    private List<CityWeatherRs> cityWeatherList;
}
