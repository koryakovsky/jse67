package ru.nlmk.study.jse67.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nlmk.study.jse67.api.CityWeatherByHoursRs;

@Service
public class WeatherApiService {

    private static final String MOSCOW_WEATHER_API_URL = "https://www.metaweather.com/api/location/2122265/";

    private final RestTemplate restTemplate;

    @Autowired
    public WeatherApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public CityWeatherByHoursRs getCityWeather() {
        System.out.println("Request moscow weather...");

        return restTemplate
                .getForEntity(MOSCOW_WEATHER_API_URL, CityWeatherByHoursRs.class).getBody();
    }

}
