package ru.nlmk.study.jse67.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ru.nlmk.study.jse67.api.CityWeatherByHoursRs;
import ru.nlmk.study.jse67.api.CityWeatherRs;
import ru.nlmk.study.jse67.dao.WeatherDao;
import ru.nlmk.study.jse67.model.Weather;

import java.util.List;

@Service
public class WeatherProcessingService {

    private final WeatherDao weatherDao;
    private final RabbitProducerService rabbitService;
    private final WeatherApiService weatherApiService;

    @Autowired
    public WeatherProcessingService(WeatherDao weatherDao, RabbitProducerService rabbitService, WeatherApiService weatherApiService) {
        this.weatherDao = weatherDao;
        this.rabbitService = rabbitService;
        this.weatherApiService = weatherApiService;
    }

    @Transactional(value = "chainedTxManager", rollbackFor = NullPointerException.class)
    public void process() {
        CityWeatherByHoursRs cityWeather = weatherApiService.getCityWeather();

        List<CityWeatherRs> cityWeatherList = cityWeather.getCityWeatherList();

        if (CollectionUtils.isEmpty(cityWeatherList)) {
            throw new IllegalStateException();
        }

        Weather weather = convertWeatherFromRs(cityWeatherList.get(0));

        weatherDao.save(weather);

        rabbitService.sendMessage(weather.toString());

        throw new NullPointerException();
    }

    private Weather convertWeatherFromRs(CityWeatherRs weatherRs) {
        Weather weather = new Weather();
        weather.setWeatherState(weatherRs.getWeatherStateName());
        weather.setHumidity(weatherRs.getHumidity());
        weather.setTemp(weatherRs.getActualTemp());
        weather.setWindDirection(weatherRs.getWindDirection());
        weather.setDateTime(weatherRs.getDate());
        weather.setWindSpeed(weatherRs.getWindSpeed());
        return weather;
    }
}
