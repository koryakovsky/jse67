package ru.nlmk.study.jse67.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "weather")
public class Weather {

    @Id
    @Column(name = "weather_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long weatherId;

    @Column(name = "weather_state", nullable = false)
    private String weatherState;

    @Column(name = "wind_direction", nullable = false)
    private String windDirection;

    @Column(name = "date_time")
    private Date dateTime;

    @Column(nullable = false)
    private Double temp;

    @Column(name = "wind_speed", nullable = false)
    private Double windSpeed;

    @Column(nullable = false)
    private Double humidity;

    @Column(nullable = false)
    private String city = "Moscow";
}
